package com.datazoom.goldcollectordemo;

import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.MediaController;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.datazoom.goldcollectordemo.databinding.ActivityMainBinding;
import com.dz.adapter.android.DZNativeAndroidAdapter;
import com.dz.collector.android.collector.DZEventCollector;
import com.dz.collector.android.connectionmanager.DZBeaconConnector;
import com.dz.collector.android.model.DatazoomConfig;
import com.dz.collector.android.model.Event;
import com.google.ads.interactivemedia.v3.api.AdEvent;
import com.google.ads.interactivemedia.v3.api.player.VideoAdPlayer;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.ext.ima.ImaAdsLoader;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.source.ads.AdsMediaSource;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import org.json.JSONArray;
import org.json.JSONException;


public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getCanonicalName();
    private ActivityMainBinding activityMainBinding;
    private MediaController mediaController;
    private int mCurrentPosition = 0;
    private static final String PLAYBACK_TIME = "play_time";
    private String url;
    private boolean mediaPlayerConfigured;
    private boolean sdkInitialized;
    private SimpleExoPlayer player;
    private ImaAdsLoader adsLoader;
    private MediaSource mediaSource;
    private DataSource.Factory dataSourceFactory;
    private String adsUrl;
    private String stagingUrl = "https://stagingplatform.datazoom.io/beacon/v1/";
    private String productionUrl = "https://platform.datazoom.io/beacon/v1/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        String configId = SharedPreference.getInstance().getValue(getApplicationContext(), SharedPreference.PREFS_KEY);
        if (!configId.isEmpty()) {
            activityMainBinding.txtConfiguration.setText(configId);
        }
        initVideoTypeSpinner();
        initAddTypesSpinner();
        initCustomDataPointSpinner();
        activityMainBinding.btnPush.setVisibility(View.GONE);
        activityMainBinding.txtVersion.setText("Demo - " + BuildConfig.VERSION_NAME + " | Library - " + BuildConfig.VERSION_NAME + " | Framework - " + BuildConfig.VERSION_NAME);
        if (savedInstanceState != null) {
            mCurrentPosition = savedInstanceState.getInt(PLAYBACK_TIME);
        }
        activityMainBinding.btnSubmit.setOnClickListener(v -> {
            if (!sdkInitialized) {
                Toast.makeText(this, " SDK is not initialized", Toast.LENGTH_LONG).show();
            } else {
                configureMediaPlayer();
            }
        });

        //new
        activityMainBinding.qaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activityMainBinding.qaButton.setBackgroundResource(R.drawable.rounded_background_white_with_border);
                activityMainBinding.productionButton.setBackgroundResource(R.drawable.round_white_background);
                activityMainBinding.connectingToUrlValue.setText(stagingUrl);
            }
        });

        activityMainBinding.productionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activityMainBinding.productionButton.setBackgroundResource(R.drawable.rounded_background_white_with_border);
                activityMainBinding.qaButton.setBackgroundResource(R.drawable.round_white_background);
                activityMainBinding.connectingToUrlValue.setText(productionUrl);
            }
        });

        activityMainBinding.initilaizeSDKSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
                if (checked && !mediaPlayerConfigured) {
                    configureSDK();
                }
            }
        });

        activityMainBinding.collectEventsSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean collectEvents) {
                DZNativeAndroidAdapter.startRecordingEvents(collectEvents);
            }
        });

        activityMainBinding.closeVideoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activityMainBinding.videoView.stopPlayback();
                activityMainBinding.videoTypeSpinner.setEnabled(true);
                activityMainBinding.closeVideoButton.setVisibility(View.GONE);
                activityMainBinding.playAd.setVisibility(View.GONE);
                activityMainBinding.videoView.setVisibility(View.INVISIBLE);
                activityMainBinding.settingsContainer.setVisibility(View.VISIBLE);
                activityMainBinding.btnPush.setVisibility(View.GONE);
                activityMainBinding.btnSubmit.setVisibility(View.VISIBLE);
                activityMainBinding.btnSubmit.setEnabled(true);
                if (player != null) {
                    player.stop();
                    player.setPlayWhenReady(false);
                    activityMainBinding.playerView.setVisibility(View.GONE);
                }
            }
        });
        activityMainBinding.configIdContainner.setOnLongClickListener(view -> {
            Toast.makeText(MainActivity.this, BuildConfig.VERSION_NAME + ": " + BuildConfig.VERSION_CODE, Toast.LENGTH_LONG).show();
            return false;
        });

        activityMainBinding.customVideoInputText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 0) {
                    activityMainBinding.videoTypeSpinner.setEnabled(false);
                    url = charSequence.toString();
                } else {
                    activityMainBinding.videoTypeSpinner.setEnabled(true);
                    url = getResources().getStringArray(R.array.video_urls)[0];
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        activityMainBinding.customAdInputTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 0) {
                    activityMainBinding.addTypeSpinner.setEnabled(false);
                    adsUrl = charSequence.toString();
                } else {
                    activityMainBinding.addTypeSpinner.setEnabled(true);
                    adsUrl = getResources().getStringArray(R.array.add_types_values)[0];
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void configureMediaPlayer() {
        activityMainBinding.btnPush.setVisibility(View.VISIBLE);
        activityMainBinding.btnSubmit.setVisibility(View.GONE);
        activityMainBinding.settingsContainer.setVisibility(View.GONE);
        activityMainBinding.btnSubmit.setEnabled(false);
        activityMainBinding.videoView.setVisibility(View.VISIBLE);
        mediaController = new MediaController(this);
        mediaController.setAnchorView(activityMainBinding.videoView);
        activityMainBinding.videoView.setMediaController(mediaController);
        activityMainBinding.videoView.setVideoURI(Uri.parse(url));
        activityMainBinding.videoView.start();
        mediaPlayerConfigured = true;
        activityMainBinding.closeVideoButton.setVisibility(View.VISIBLE);
        new Handler().postDelayed(() -> {
            activityMainBinding.playAd.setVisibility(View.VISIBLE);
            activityMainBinding.playAd.setOnClickListener(view -> {
                activityMainBinding.playAd.setVisibility(View.GONE);
                playAd();
            });
        }, 5000);

    }

    private void setupCustomMetadata() {
        try {
            DZEventCollector.setCustomMetadata(new JSONArray("["
                            + "{\"custom_player_name\": \"Android Gold Media Player\"},"
                            + "{\"custom_domain\": \"demo.datazoom.io\"}"
                            + "]"
            ));
        } catch (JSONException e) {
            Log.e(TAG, "Error setting custom metadata", e);
        }



    }

    private void setupCustomEvent(DZEventCollector dzEventCollector) {
        Event event = new Event("Datazoom_GOLD_SDK_Loaded", new JSONArray());
        dzEventCollector.addCustomEvent(event);
        activityMainBinding.btnPush.setOnClickListener(v -> {
            Event event1 = new Event("buttonClick", new JSONArray());
            dzEventCollector.addCustomEvent(event1);
        });
    }

    private void configureSDK() {
        String configId = activityMainBinding.txtConfiguration.getText().toString();
        String configUrl = activityMainBinding.connectingToUrlValue.getText().toString();
        DZNativeAndroidAdapter
                .createContextAndPlayer(this, activityMainBinding.videoView)
                .setConfig(new DatazoomConfig(configId, configUrl))
                .connect(new DZBeaconConnector.ConnectionListener() {
                             @Override
                             public void onSuccess(DZEventCollector dzEventCollector) {
                                 setupCustomMetadata();
                                 sdkInitialized = true;
                                 setupCustomEvent(dzEventCollector);
                                 DZNativeAndroidAdapter.startRecordingEvents(true);
                                 DZNativeAndroidAdapter.addIMAObservers(adsLoader);
                                 activityMainBinding.btnPush.setOnClickListener(v -> {
                                     try {
                                         Event event1 = new Event("btnPush", new JSONArray("[{\"customPlay\": \"true\"}]  "));
                                         dzEventCollector.addCustomEvent(event1);
                                     } catch (JSONException e) {
                                         e.printStackTrace();
                                     }
                                 });
                             }

                             @Override
                             public void onError(Throwable t) {
                                 activityMainBinding.btnSubmit.setEnabled(true);
                                 showAlert("Error", "Error while creating NativeAndroidConnector, error:" + t.getMessage());
                             }
                         }
                );
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(PLAYBACK_TIME, activityMainBinding.videoView.getCurrentPosition());
    }

    private void initializePlayer() {
        if (mCurrentPosition > 0) {
            activityMainBinding.videoView.seekTo(mCurrentPosition);
        }
    }




    @Override
    protected void onStart() {
        super.onStart();
        initializePlayer();
    }

    private void playAd() {
        if (activityMainBinding.addTypeSpinner.getSelectedItem().toString().equalsIgnoreCase(getString(R.string.none)) && activityMainBinding.customAdInputTxt.getText().toString().isEmpty()) {
            Toast.makeText(this, "Invalid ad type", Toast.LENGTH_LONG).show();
        } else {
            initializeAdPlayer();
            AdsMediaSource adsMediaSource = new AdsMediaSource(mediaSource, dataSourceFactory, adsLoader, activityMainBinding.playerView);
            player.prepare(adsMediaSource);
        }
    }

    private void initializeAdPlayer() {
        adsLoader = new ImaAdsLoader(this, Uri.parse(adsUrl));
        DZNativeAndroidAdapter.addIMAObservers(adsLoader);
        adsLoader.addCallback(new VideoAdPlayer.VideoAdPlayerCallback() {
            @Override
            public void onPlay() {
            }

            @Override
            public void onVolumeChanged(int i) {

            }

            @Override
            public void onPause() {

            }

            @Override
            public void onLoaded() {

            }

            @Override
            public void onResume() {

            }

            @Override
            public void onEnded() {
                player.stop();
                player.setPlayWhenReady(false);
                activityMainBinding.playerView.setVisibility(View.GONE);
                activityMainBinding.videoView.setVisibility(View.VISIBLE);
                if (mCurrentPosition > 0) {
                    activityMainBinding.videoView.seekTo(mCurrentPosition);
                }
                activityMainBinding.videoTypeSpinner.setEnabled(true);
                activityMainBinding.videoView.start();

                adsUrl = getString(R.string.ad_tag_single_inline_linear_url);
                new Handler().postDelayed(() -> {
                    if (!activityMainBinding.addTypeSpinner.getSelectedItem().toString().equalsIgnoreCase(getString(R.string.none))) {
                        activityMainBinding.playAd.setVisibility(View.VISIBLE);
                    }
                }, 3000);
            }

            @Override
            public void onError() {
            }

            @Override
            public void onBuffering() {

            }
        });
        adsLoader.getAdsLoader().addAdsLoadedListener(adsManagerLoadedEvent -> {
            adsManagerLoadedEvent.getAdsManager().addAdEventListener(new AdEvent.AdEventListener() {
                @Override
                public void onAdEvent(AdEvent adEvent) {
                    switch (adEvent.getType()) {
                        case CLICKED:
                            adsLoader.pauseAd();
                            player.setPlayWhenReady(false);
                            break;
                    }
                }
            });
            mCurrentPosition = activityMainBinding.videoView.getCurrentPosition();
            activityMainBinding.videoView.pause();
            activityMainBinding.videoView.setVisibility(View.GONE);
            activityMainBinding.playerView.setVisibility(View.VISIBLE);
            activityMainBinding.videoTypeSpinner.setEnabled(false);
            player.setPlayWhenReady(true);
        });

        player = new SimpleExoPlayer.Builder(this).build();
        activityMainBinding.playerView.setPlayer(player);
        adsLoader.setPlayer(player);
        dataSourceFactory = new DefaultDataSourceFactory(this, Util.getUserAgent(this, getString(R.string.app_name)));
        ProgressiveMediaSource.Factory mediaSourceFactory = new ProgressiveMediaSource.Factory(dataSourceFactory);
        mediaSource = mediaSourceFactory.createMediaSource(Uri.parse(getString(R.string.content_url)));
        player.prepare(mediaSource);
        player.setPlayWhenReady(false);
    }

    private void releaseIMAPlayer() {
        if (adsLoader != null) {
            adsLoader.setPlayer(null);
        }
        activityMainBinding.playerView.setPlayer(null);
        if (player != null) {
            player.release();
            player = null;
        }

    }

    private void stopVideoPlayer() {
        if (activityMainBinding.videoView != null) {
            if (mediaPlayerConfigured) {
                activityMainBinding.videoView.stopPlayback();
                activityMainBinding.videoView.pause();
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onStop() {
        super.onStop();
        stopVideoPlayer();
    }

    @Override
    protected void onPause() {
        super.onPause();
        storeConfigID();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        releaseIMAPlayer();
        DZNativeAndroidAdapter.releasePlayer();
    }

    private void storeConfigID() {
        SharedPreference.getInstance().save(this, activityMainBinding.txtConfiguration.getText().toString(), SharedPreference.PREFS_KEY);
    }

    private void showAlert(String title, String message) {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder.setTitle(title)
                .setMessage(message)
                .setPositiveButton(android.R.string.yes, (dialog, which) -> {
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    private void initVideoTypeSpinner() {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.video_types));
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        activityMainBinding.videoTypeSpinner.setAdapter(adapter);
        // Default value
        url = getResources().getStringArray(R.array.video_urls)[0];
        checkCustomVideoUrl();
        activityMainBinding.videoTypeSpinner.setSelection(0, false);
        activityMainBinding.videoTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                url = getResources().getStringArray(R.array.video_urls)[position];
                checkCustomVideoUrl();
                if (mediaPlayerConfigured) {
                    activityMainBinding.customVideoInputText.clearFocus();
                    activityMainBinding.videoView.setVideoURI(Uri.parse(url));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }
        });
    }

    private void initCustomDataPointSpinner() {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.custom_datapoint_array));
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        activityMainBinding.customDataPointSpinner.setAdapter(adapter);
        activityMainBinding.customDataPointSpinner.setSelection(0, false);
        activityMainBinding.customDataPointSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                if (mediaPlayerConfigured && position > 0) {
                    if (activityMainBinding.videoView.isPlaying()) {
                        activityMainBinding.videoView.pause();
                    }
                    handleCustomDataPointAction(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }
        });
    }

    private void handleCustomDataPointAction(int action) {
        switch (action) {
            case CMConstants.ADD_APP_SESSION_METADATA:
                CMDialogHandler.addAppSessionMetadataDialog(this);
                activityMainBinding.customDataPointSpinner.setSelection(0, false);
                break;
            case CMConstants.ADD_PLAYER_SESSION_METADATA:
                CMDialogHandler.addPlayerSessionMetadataDialog(this);
                activityMainBinding.customDataPointSpinner.setSelection(0, false);
                break;
            case CMConstants.READ_APP_SESSION_METADATA:
                Toast.makeText(this, DZEventCollector.getAppSessionCustomMetadata().toString(), Toast.LENGTH_LONG).show();
                activityMainBinding.customDataPointSpinner.setSelection(0, false);
                break;
            case CMConstants.READ_PLAYER_SESSION_METADATA:
                Toast.makeText(this, DZEventCollector.getPlayerSessionCustomMetadata().toString(), Toast.LENGTH_LONG).show();
                activityMainBinding.customDataPointSpinner.setSelection(0, false);
                break;
            case CMConstants.DELETE_APP_SESSION_METADATA:
                DZEventCollector.clearAppSessionCustomMetadata();
                activityMainBinding.customDataPointSpinner.setSelection(0, false);
                break;
            case CMConstants.DELETE_PLAYER_SESSION_METADATA:
                DZEventCollector.clearPlayerSessionCustomMetadata();
                activityMainBinding.customDataPointSpinner.setSelection(0, false);
                break;
            case CMConstants.UPDATE_APP_SESSION_METADATA:
                CMDialogHandler.updateAppSessionMetadataDialog(this);
                activityMainBinding.customDataPointSpinner.setSelection(0, false);
                break;
            case CMConstants.UPDATE_PLAYER_SESSION_METADATA:
                CMDialogHandler.updatePlayerSessionMetadataDialog(this);
                activityMainBinding.customDataPointSpinner.setSelection(0, false);
                break;
            case CMConstants.ADD_CUSTOM_METADATA_FOR_EXPLICINT_EVENT:
                CMDialogHandler.addMetadataForExplicitEventDialog(this);
                activityMainBinding.customDataPointSpinner.setSelection(0, false);
                break;
            case CMConstants.DELETE_CUSTOM_METADATA_FOR_EXPLICINT_EVENT:
                DZEventCollector.clearMetadataForAppropriateEvent();
                activityMainBinding.customDataPointSpinner.setSelection(0, false);
                break;

        }

    }

    private void checkCustomVideoUrl() {
        if (!activityMainBinding.customVideoInputText.getText().toString().isEmpty()) {
            url = activityMainBinding.customVideoInputText.getText().toString();
        }
    }

    private void checkCustomAdUrl() {
        if (!activityMainBinding.customAdInputTxt.getText().toString().isEmpty()) {
            adsUrl = activityMainBinding.customAdInputTxt.getText().toString();
        } else {
            activityMainBinding.customAdInputTxt.clearFocus();
        }
    }

    private void initAddTypesSpinner() {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.add_types_titles));
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        activityMainBinding.addTypeSpinner.setAdapter(adapter);
        // Default value
        adsUrl = getResources().getStringArray(R.array.add_types_values)[0];
        checkCustomAdUrl();
        activityMainBinding.addTypeSpinner.setSelection(0, false);
        activityMainBinding.addTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                adsUrl = getResources().getStringArray(R.array.add_types_values)[position];
                checkCustomAdUrl();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }
        });

    }
}